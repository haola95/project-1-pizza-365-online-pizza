const express = require('express');


// Import mongooseJS
const mongoose = require("mongoose");

//Import Router
const drinkRouter = require('./app/routes/drinkRouter.js')
const voucherRouter = require('./app/routes/voucherRouter.js')
const userRouter = require('./app/routes/userRouter.js')
const orderRouter = require('./app/routes/orderRouter.js')
const pizza365Router = require('./app/routes/pizza365Router.js')
// khởi tạo app express
const app = express();

// Khai báo middleware đọc Json
app.use(express.json());

//Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded ({
    extended: true
}))

// khai báo cổng của project
const port = 8000;

mongoose.connect("mongodb://localhost:27017/CRUD_Pizza365", (err) => {
    if(err) {
        throw err;
    };
    console.log("Connect MongoDB successfully!");
})

app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
(request, response, next) => {
    console.log("Request method", request.method);
    next();
})
// khai báo Api dạng get "/" sẽ chạy vào đây
// Call back funtion: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm được gọi
app.get("/hello-customer", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Xin chào customer, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
  
})
app.use("/", drinkRouter);
app.use("/", voucherRouter);
app.use("/", userRouter);
app.use("/", orderRouter);
app.use("/", pizza365Router);

// chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port)
})