/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gSelectedSizeMenu = {
    menuName: "",    // S, M, L
    duongKinhCM: "",
    suonnuong: "",
    saladGr: "",
    drink: "",
    priceVND: ""
}
var gSelectPizzaType = {
    pizzaType: ""
}

var gPercent = 0;
var gVoucher = 0;
var gPrice = 0;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    "use strict";
    onPageLoading();
    $("#btn-size-small").on("click", function () {
        onBtnSmallClick()
    });
    $("#btn-size-medium").on("click", function () {
        onBtnMediumClick()
    });
    $("#btn-size-large").on("click", function () {
        onBtnLargeClick()
    });
    $("#btn-chon-hai-san").on("click", function () {
        onBtnChooseHaiSanClick()
    });
    $("#btn-chon-hawai").on("click", function () {
        onBtnChooseHawaiiClick()
    });
    $("#btn-chon-thit-xong-khoi").on("click", function () {
        onBtnChooseThitXongKhoiClick()
    });
    $("#btn-gui-don").on("click", function () {
        onBtnGuiDonClick()
    });
    $("#btn-create-order").on("click", function () {
        onBtnCreateOrderModalClick()
    });
    $("#btn-back").on("click", function () {
        onBtnBackOrderModalClick()
    });


});


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm thực hiện khi load trang
function onPageLoading() {
    "use strict"
    console.log("Load trang")
    // gọi Api load dữ liệu nước uống
    getApiDataDrinkList();
}
// hàm thực hiện khi ấn chọn pizza size S
function onBtnSmallClick() {
    "use strict"
    // khai báo dữ liệu
    getDataChoosePizzaSizeS(gSelectedSizeMenu);
    // hiển thị data ra console
    showDataChoosePizzaSizSOnConsole(gSelectedSizeMenu)
    // đồi màu button khi chọn
    changColorButtonMenu(gSelectedSizeMenu.menuName)
}
// hàm thực hiện khi ấn chọn pizza size M
function onBtnMediumClick() {
    "use strict"
    // khai báo dữ liệu
    getDataChoosePizzaSizeM(gSelectedSizeMenu);
    // hiển thị data ra console
    showDataChoosePizzaSizMOnConsole(gSelectedSizeMenu)
    // đồi màu button khi chọn
    changColorButtonMenu(gSelectedSizeMenu.menuName)
}
// hàm thực hiện khi ấn chọn pizza size L
function onBtnLargeClick() {
    "use strict"
    // khai báo dữ liệu
    getDataChoosePizzaSizeL(gSelectedSizeMenu);
    // hiển thị data ra console
    showDataChoosePizzaSizLOnConsole(gSelectedSizeMenu)
    // đồi màu button khi chọn
    changColorButtonMenu(gSelectedSizeMenu.menuName)
}
// hàm thực hiện khi ấn chọn pizza OCEAN MANIA
function onBtnChooseHaiSanClick() {
    "use strict"
    gSelectPizzaType.pizzaType = "OCEAN MANIA";
    console.log("CHỌN LOẠI PIZZA");
    console.log("PIZZA" + " " + gSelectPizzaType.pizzaType);
    // đồi màu button khi chọn
    changColorButtonPizza(gSelectPizzaType.pizzaType)
}
// hàm thực hiện khi ấn chọn pizza size HAWAIIAN
function onBtnChooseHawaiiClick() {
    "use strict"
    gSelectPizzaType.pizzaType = "HAWAIIAN";
    console.log("CHỌN LOẠI PIZZA");
    console.log("PIZZA" + " " + gSelectPizzaType.pizzaType);
    // đồi màu button khi chọn
    changColorButtonPizza(gSelectPizzaType.pizzaType)
}
// hàm thực hiện khi ấn chọn pizza CHESSE CHICKEN BACON
function onBtnChooseThitXongKhoiClick() {
    "use strict"
    gSelectPizzaType.pizzaType = "CHESSE CHICKEN BACON";
    console.log("CHỌN LOẠI PIZZA");
    console.log("PIZZA" + " " + gSelectPizzaType.pizzaType);
    // đồi màu button khi chọn
    changColorButtonPizza(gSelectPizzaType.pizzaType)
}
// hàm thực hiện khi ấn gửi đơn
function onBtnGuiDonClick() {
    "use strict"
    // khai báo dữ liệu
    var vCustomer = {
        fullname: "",
        email: "",
        dienthoai: "",
        diachi: "",
        magiamgia: -1,
        loinhan: "",
    }
    // bước 1: thu thập dữ liệu
    getDataCustomer(vCustomer)
    // bước 2: kiểm tra
    var vCheck = validateDataCustomer(vCustomer)
    if (vCheck) {
        // bước 3: gọi api
        $("#create-order-modal").modal("show");
        checkVoucher(vCustomer)
    }
    // bước 4: hiển thị sau khi send request

}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm khai báo dữ liệu chọn pizza size S
function getDataChoosePizzaSizeS(paramSelectedSizeMenu) {
    "use strict"
    paramSelectedSizeMenu.menuName = "Small";
    paramSelectedSizeMenu.duongKinhCM = "20cm";
    paramSelectedSizeMenu.suonnuong = 2;
    paramSelectedSizeMenu.saladGr = "200g";
    paramSelectedSizeMenu.drink = 2;
    paramSelectedSizeMenu.priceVND = "150000";

}
// hàm hiển thị data chọn size S ra console
function showDataChoosePizzaSizSOnConsole(paramSelectedSizeMenu) {
    "use strict"
    console.log("CHỌN SIZE PIZZA");
    console.log("Size: " + paramSelectedSizeMenu.menuName);
    console.log("Đường kính: " + paramSelectedSizeMenu.duongKinhCM);
    console.log("Sườn nướng: " + paramSelectedSizeMenu.suonnuong);
    console.log("Salad: " + paramSelectedSizeMenu.saladGr);
    console.log("Nước ngọt: " + paramSelectedSizeMenu.drink);
    console.log("Giá tiền: " + paramSelectedSizeMenu.priceVND + " VND");

}
// hàm khai báo dữ liệu chọn pizza size M
function getDataChoosePizzaSizeM(paramSelectedSizeMenu) {
    "use strict"
    paramSelectedSizeMenu.menuName = "Medium";
    paramSelectedSizeMenu.duongKinhCM = "25cm";
    paramSelectedSizeMenu.suonnuong = 4;
    paramSelectedSizeMenu.saladGr = "300g";
    paramSelectedSizeMenu.drink = 3;
    paramSelectedSizeMenu.priceVND = "200000";

}
// hàm hiển thị data chọn size M ra console
function showDataChoosePizzaSizMOnConsole(paramSelectedSizeMenu) {
    "use strict"
    console.log("CHỌN SIZE PIZZA");
    console.log("Size: " + paramSelectedSizeMenu.menuName);
    console.log("Đường kính: " + paramSelectedSizeMenu.duongKinhCM);
    console.log("Sườn nướng: " + paramSelectedSizeMenu.suonnuong);
    console.log("Salad: " + paramSelectedSizeMenu.saladGr);
    console.log("Nước ngọt: " + paramSelectedSizeMenu.drink);
    console.log("Giá tiền: " + paramSelectedSizeMenu.priceVND + " VND");

}
// hàm khai báo dữ liệu chọn pizza size L
function getDataChoosePizzaSizeL(paramSelectedSizeMenu) {
    "use strict"
    paramSelectedSizeMenu.menuName = "Large";
    paramSelectedSizeMenu.duongKinhCM = "30cm";
    paramSelectedSizeMenu.suonnuong = 8;
    paramSelectedSizeMenu.saladGr = "500g";
    paramSelectedSizeMenu.drink = 4;
    paramSelectedSizeMenu.priceVND = "250000";

}
// hàm hiển thị data chọn size L ra console
function showDataChoosePizzaSizLOnConsole(paramSelectedSizeMenu) {
    "use strict"
    console.log("CHỌN SIZE PIZZA");
    console.log("Size: " + paramSelectedSizeMenu.menuName);
    console.log("Đường kính: " + paramSelectedSizeMenu.duongKinhCM);
    console.log("Sườn nướng: " + paramSelectedSizeMenu.suonnuong);
    console.log("Salad: " + paramSelectedSizeMenu.saladGr);
    console.log("Nước ngọt: " + paramSelectedSizeMenu.drink);
    console.log("Giá tiền: " + paramSelectedSizeMenu.priceVND + " VND");

}
// hàm call api lấy danh sách nước uống
function getApiDataDrinkList() {
    "use strict"
    // mở và gửi request đi
    $.ajax({
        url: "/devcamp-pizza365/drinks",
        type: 'GET',
        dataType: 'json', // added data type
        success: function (DrinkList) {
            // gọi hàm hiển thị thông tin nước uống lên select drink
            showDrinklistToSelect(DrinkList)
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
// hàm hiển thị thông tin nước uống lên select drink
function showDrinklistToSelect(paramDrinkList) {
    "use strict"
    for (var bI = 0; bI < paramDrinkList.length; bI++) {
        $("#select_drink").append($("<option>", {
            value: paramDrinkList[bI].maNuocUong,
            text: paramDrinkList[bI].tenNuocUong,
        }))
    }
}
// hàm thu thập dữ liệu khách hàng
function getDataCustomer(paramCustomer) {
    "use strict"
    // truy xuất phần tử HTML
    var vInpFullName = $("#inp-fullname");
    var vInpEmail = $("#inp-email");
    var vInpNumberPhone = $("#inp-dien-thoai");
    var vInpAddress = $("#inp-dia-chi");
    var vInpMessage = $("#inp-message");
    var vInpVoucherDiscount = $("#inp-ma-giam-gia");
    // lấy và lưu dữ liệu vào Obj
    paramCustomer.fullname = vInpFullName.val().trim();
    paramCustomer.email = vInpEmail.val().trim();
    paramCustomer.dienthoai = vInpNumberPhone.val().trim();
    paramCustomer.diachi = vInpAddress.val().trim();
    paramCustomer.loinhan = vInpMessage.val().trim();
    paramCustomer.magiamgia = vInpVoucherDiscount.val().trim();
}
// hàm kiểm tra dữ liệu khách hàng
function validateDataCustomer(paramCustomer) {
    var regexEmail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var regexNumberphone = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;


    if(gSelectedSizeMenu.menuName == "") {
        alert("Vui lòng chọn Size Pizza !!!");
        return false;
    }
    if(gSelectPizzaType.pizzaType == "") {
        alert("Vui lòng chọn loại Pizza !!!");
        return false;
    }
    if (paramCustomer.fullname === "") {
        alert("Vui lòng nhập họ và tên !!!");
        return false;
    }
    if (paramCustomer.email === "") {
        alert("Vui lòng nhập email !!!");
        return false;
    }
    if (!regexEmail.test(paramCustomer.email)) {
        alert("Email được nhập không hợp lệ !!!");
        return false;
    }
    if (paramCustomer.dienthoai === "") {
        alert("Vui lòng nhập số điện thoại !!!");
        return false;
    }
    if (!regexNumberphone.test(paramCustomer.dienthoai)) {
        alert("Số điện thoại không hợp lệ !!!");
        return false;
    }
    if (paramCustomer.diachi === "") {
        alert("Vui lòng nhập địa chỉ !!!");
        return false;
    }
    if(($("#select_drink").val()) === "all") {
        alert("Vui lòng chọn nước uống !!!");
        return false;
    }
    return true;
}
// hàm kiểm tra mã voucher
function checkVoucher(pamramCustomer) {
    "use strict"
    // khai báo dữ liệu
    // bước 1 : thu thập dữ liệu
    gVoucher = $("#inp-ma-giam-gia").val().trim()
    // bước 2 : kiểm tra
    // bước 3: gọi api
    callApiCheckVoucherByVId(gVoucher, pamramCustomer)
    // bước 4: hiển thị sau send request
}
// hàm gọi api check voucher
function callApiCheckVoucherByVId(paramVoucher, pamramCustomer) {
    "use strict"
    // mở và gửi request đi
    $.ajax({
        url: "/devcamp-pizza365/vouchers/" + paramVoucher,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (VoucherObj) {
            // lấy thông tin phần trăm giảm giá
            gPercent = VoucherObj.phanTramGiamGia;
            // hiển thị thông tin lên order modal
            showDataOnOrderModal(pamramCustomer);
            console.log("Voucher" + VoucherObj)
            console.log("Phần trăm giảm giá: " + gPercent + " %")
        },
        error: function () {
            showDataOnOrderModalApiError(pamramCustomer);
            console.log("Không có mã hoặc sai")
            console.log("Phần trăm giảm giá: " + gPercent + " %")
        }
    });

}
// hàm hiển thị lên order Modal
function showDataOnOrderModal(paramCustomer) {
    "use strict";
    var vPrice = gSelectedSizeMenu.priceVND - ((gSelectedSizeMenu.priceVND) * (gPercent / 100));
    $("#inp-create-size").val(gSelectedSizeMenu.menuName).prop('disabled', true);
    $("#inp-create-duong-kinh").val(gSelectedSizeMenu.duongKinhCM).prop('disabled', true)
    $("#inp-create-suon-nuong").val(gSelectedSizeMenu.suonnuong).prop('disabled', true)
    $("#inp-create-salad").val(gSelectedSizeMenu.saladGr).prop('disabled', true)
    $("#inp-create-loai-nuoc").val($("#select_drink").val()).prop('disabled', true)
    $("#inp-create-so-luong").val(gSelectedSizeMenu.drink).prop('disabled', true)

    $("#input-create-pizza-type").val(gSelectPizzaType.pizzaType).prop('disabled', true)
    $("#input-create-voucher").val(paramCustomer.magiamgia).prop('disabled', true)
    $("#input-create-price").val(gSelectedSizeMenu.priceVND + "" + "VND").prop('disabled', true)
    $("#input-create-discount").val(gPercent + "%").prop('disabled', true)
    $("#input-create-thanh-toan").val(vPrice + "" + "VND").prop('disabled', true)

    $("#inp-create-name").val(paramCustomer.fullname).prop('disabled', true)
    $("#inp-create-mobile").val(paramCustomer.dienthoai).prop('disabled', true)
    $("#inp-create-email").val(paramCustomer.email).prop('disabled', true)
    $("#inp-create-address").val(paramCustomer.diachi).prop('disabled', true)
    $("#input-create-message").val(paramCustomer.loinhan).prop('disabled', true)

}

function showDataOnOrderModalApiError(paramCustomer) {
    "use strict";
    $("#inp-create-size").val(gSelectedSizeMenu.menuName).prop('disabled', true);
    $("#inp-create-duong-kinh").val(gSelectedSizeMenu.duongKinhCM).prop('disabled', true)
    $("#inp-create-suon-nuong").val(gSelectedSizeMenu.suonnuong).prop('disabled', true)
    $("#inp-create-salad").val(gSelectedSizeMenu.saladGr).prop('disabled', true)
    $("#inp-create-loai-nuoc").val($("#select_drink").val()).prop('disabled', true)
    $("#inp-create-so-luong").val(gSelectedSizeMenu.drink).prop('disabled', true)

    $("#input-create-pizza-type").val(gSelectPizzaType.pizzaType).prop('disabled', true)
    $("#input-create-voucher").val(paramCustomer.magiamgia ).prop('disabled', true)
    $("#input-create-price").val(gSelectedSizeMenu.priceVND + " " + "VND").prop('disabled', true)
    $("#input-create-discount").val(gPercent + "%").prop('disabled', true)
    $("#input-create-thanh-toan").val(gSelectedSizeMenu.priceVND + "VND").prop('disabled', true)

    $("#inp-create-name").val(paramCustomer.fullname).prop('disabled', true)
    $("#inp-create-mobile").val(paramCustomer.dienthoai).prop('disabled', true)
    $("#inp-create-email").val(paramCustomer.email).prop('disabled', true)
    $("#inp-create-address").val(paramCustomer.diachi).prop('disabled', true)
    $("#input-create-message").val(paramCustomer.loinhan).prop('disabled', true)

}
// hàm thực hiện khi ấn tạo đơn modal
function onBtnCreateOrderModalClick() {
    "use strict";
    // khai báo đối tượng
    var vObjectRequest = {
        menuName: "",
        duongKinh: "",
        suon: "",
        salad: "",
        pizzaType: "",
        maVoucher: "",
        maNuocUong: "",
        soLuongNuoc: "",
        fullName: "",
        priceVND: "",
        email: "",
        phone: "",
        address: "",
        loiNhan: ""
    }
    // bước 1: thu thập dữ liệu
    getDataObjectRequest(vObjectRequest)
    // bước 2: kiểm tra
    // bước 3: gọi api tạo order mới
    callApiCreateOrder(vObjectRequest)
    // bước 4: hiển thị sau khi send request 
}

// hàm thu thập dự liệu của onBtnCreateOrderModalClick
function getDataObjectRequest(paramObjectRequest) {
    "use strict";
    paramObjectRequest.menuName = $("#inp-create-size").val();
    paramObjectRequest.duongKinh = $("#inp-create-duong-kinh").val();
    paramObjectRequest.suon = $("#inp-create-suon-nuong").val();
    paramObjectRequest.salad = $("#inp-create-salad").val();
    paramObjectRequest.pizzaType = $("#input-create-pizza-type").val();
    paramObjectRequest.maVoucher = $("#input-create-voucher").val();
    paramObjectRequest.maNuocUong = $("#inp-create-loai-nuoc").val();
    paramObjectRequest.soLuongNuoc = $("#inp-create-so-luong").val();
    paramObjectRequest.fullName = $("#inp-create-name").val();
    paramObjectRequest.priceVND = $("#inp-create-thanh-toan").val();
    paramObjectRequest.email = $("#inp-create-email").val();
    paramObjectRequest.phone = $("#inp-create-mobile").val();
    paramObjectRequest.address = $("#inp-create-address").val();
    paramObjectRequest.loiNhan = $("#inp-create-message").val();
}
// hàm gọi api tạo order mới
function callApiCreateOrder(paramObjectRequest) {
    "use strict";
    $.ajax({
        url: "/devcamp-pizza365/orders",
        type: 'POST',
        dataType: 'json', // added data type
        contentType: 'application/json',
        data: JSON.stringify(paramObjectRequest),
        success: function (paramObjectRequest) {
            console.log(paramObjectRequest);
            // gọi hàm hiển thị thông tin Order thành công
            showOrderSuccessOnModal(paramObjectRequest)
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
// hàm hiển thị mã đơn hàng
function showOrderSuccessOnModal(paramNewOrderSuccess) {
    "use strict";
    $("#order-success-modal").modal("show");
    console.log(paramNewOrderSuccess.orderId)
    $("#input-ma-don-hang").val(paramNewOrderSuccess.orderId).prop('disabled', true);


}
// hàm thực hiện khi ấn back modal
function onBtnBackOrderModalClick() {
    "use strict";
    $("#create-order-modal").modal("hide");
}
// hàm thay đồi màu khi chọn Menu
function changColorButtonMenu(paramMenuName) {
    "use strict";
    var vBtnSmall = document.getElementById("btn-size-small");  // truy vấn nút gói small (sign - up 0 )
  var vBtnMeditum = document.getElementById("btn-size-medium"); //truy vấn nút gói medium (sign - up ) 
  var vBtnLarge = document.getElementById("btn-size-large");
  
  if (paramMenuName === "Small") {  //nếu chọn Small thì thay màu nút Chọn bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    //code đỏi màu 3 nút, khi gói Small được chọn
    vBtnSmall.className = "btn btn-success w-100";
    vBtnMeditum.className = "btn btn-warning w-100";
    vBtnLarge.className = "btn btn-warning w-100";
  }
  else if (paramMenuName === "Medium") {  //nếu chọn Meditum thì thay màu nút Chọn bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    vBtnSmall.className = "btn btn-warning w-100";
    vBtnMeditum.className = "btn btn-success w-100";
    vBtnLarge.className = "btn btn-warning w-100";
  } 
  else if (paramMenuName === "Large") { //nếu chọn Large thì thay màu nút Premium bàng màu cam, hai nút kia xanh
    //code đỏi màu 3 nút, khi gói Large được chọn
    vBtnSmall.className = "btn btn-warning w-100";
    vBtnMeditum.className = "btn btn-warning w-100";
    vBtnLarge.className = "btn btn-success w-100";
  }
}
// hàm thay đồi màu khi chọn Pizza
function changColorButtonPizza(paramPizzaType) {
    "use strict";
    var vBtnHawai = document.getElementById("btn-chon-hawai");
    var vBtnHaiSan = document.getElementById("btn-chon-hai-san");
    var vBtnThitXongKhoi = document.getElementById("btn-chon-thit-xong-khoi");
  
    if (paramPizzaType === "HAWAIIAN") {  //nếu chọn Small thì thay màu nút Chọn bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
        //code đỏi màu 3 nút, khi gói Small được chọn
        vBtnHawai.className = "btn btn-success w-100";
        vBtnHaiSan.className = "btn btn-warning w-100";
        vBtnThitXongKhoi.className = "btn btn-warning w-100";
      }
      else if (paramPizzaType === "OCEAN MANIA") {  //nếu chọn Meditum thì thay màu nút Chọn bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
        vBtnHawai.className = "btn btn-warning w-100";
        vBtnHaiSan.className = "btn btn-success w-100";
        vBtnThitXongKhoi.className = "btn btn-warning w-100";
      } 
      else if (paramPizzaType === "CHESSE CHICKEN BACON") { //nếu chọn Large thì thay màu nút Premium bàng màu cam, hai nút kia xanh
        //code đỏi màu 3 nút, khi gói Large được chọn
        vBtnHawai.className = "btn btn-warning w-100";
        vBtnHaiSan.className = "btn btn-warning w-100";
        vBtnThitXongKhoi.className = "btn btn-success w-100";
      }
    }

