// B1: Import mongooseJS
const mongoose = require("mongoose");

// B2: Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// B3: Khởi tạo 1 schema với các thuộc tính được yêu cầu
const orderSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    orderCode: {
        type: String,
        default: 0,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: [{
        type: mongoose.Types.ObjectId,
        ref: "voucher"
    }],
    drink: [{
        type: mongoose.Types.ObjectId,
        ref: "drink"
    }],
    status: {
        type: String,
        required: true
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});

// B4: Export ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("order", orderSchema);


