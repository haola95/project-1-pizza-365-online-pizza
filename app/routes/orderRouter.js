// Import bo thu vien express
const express = require('express');

const {createOrderOfUser, getAllOrderOfUser, getOrderById, updateOrderById, deleteOrderById} = require("../controllers/orderController.js")

// Import Course Middleware
const { printCourseURLMiddleware } = require('../middlewares/courseMiddlewares.js');

const router = express.Router();

router.get("/users/:userId/orders", printCourseURLMiddleware, getAllOrderOfUser);

router.post("/users/:userId/orders", printCourseURLMiddleware, createOrderOfUser);

router.get("/orders/:orderId", printCourseURLMiddleware, getOrderById);

router.put("/orders/:orderId", printCourseURLMiddleware, updateOrderById);

router.delete("/users/:userId/orders/:orderId", printCourseURLMiddleware, deleteOrderById);
// Export dữ liệu 1 module
module.exports = router;