// Import bo thu vien express
const express = require('express');

const {createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require("../controllers/userController.js")

// Import Course Middleware
const { printCourseURLMiddleware } = require('../middlewares/courseMiddlewares.js');

const router = express.Router();

router.get("/users", printCourseURLMiddleware, getAllUser);

router.post("/users", printCourseURLMiddleware, createUser);

router.get("/users/:userId", printCourseURLMiddleware, getUserById);

router.put("/users/:userId", printCourseURLMiddleware, updateUserById);

router.delete("/users/:userId", printCourseURLMiddleware, deleteUserById);

// Export dữ liệu 1 module
module.exports = router;