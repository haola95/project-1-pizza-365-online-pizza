// Import bo thu vien express
const express = require('express');

// Import Course Middleware
const { printCourseURLMiddleware } = require('../middlewares/courseMiddlewares.js');

const router = express.Router();

// Import drink Controller
const { createVoucherOfOrder, getAllVoucherOfOrder, getVoucherById, updateVoucherById, deleteVoucherById} = require("../controllers/voucherController.js")

router.get("/orders/:orderId/vouchers", printCourseURLMiddleware, getAllVoucherOfOrder);

router.post("/orders/:orderId/vouchers", printCourseURLMiddleware, createVoucherOfOrder);

router.get("/vouchers/:voucherId", printCourseURLMiddleware, getVoucherById);

router.put("/vouchers/:voucherId", printCourseURLMiddleware, updateVoucherById);

router.delete("/orders/:orderId/vouchers/:voucherId", printCourseURLMiddleware, deleteVoucherById);

// Export dữ liệu 1 module
module.exports = router;