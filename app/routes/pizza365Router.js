// Import bo thu vien express
const express = require('express');

const {createOrder, createDrink, getAllDrink, getAllOrder, getAllVoucher, getVoucherByMaVoucher, createVoucher, getAllUser} = require("../controllers/pizza365Controller.js")

const path = require("path")

// Import Course Middleware
const { printCourseURLMiddleware} = require('../middlewares/courseMiddlewares.js');

const router = express.Router();

router.use(express.static(__dirname + "../../../views"))

router.get("/", (request, response) => {
    console.log(__dirname)
    response.sendFile(path.join(__dirname+'../../../views/Pizza 365 version v1.11.html'));
})

router.post("/devcamp-pizza365/orders", printCourseURLMiddleware, createOrder);

router.get("/devcamp-pizza365/orders", printCourseURLMiddleware, getAllOrder);

router.post("/devcamp-pizza365/drinks", printCourseURLMiddleware, createDrink);

router.get("/devcamp-pizza365/drinks", printCourseURLMiddleware, getAllDrink);

router.post("/devcamp-pizza365/vouchers", printCourseURLMiddleware, createVoucher);

router.get("/devcamp-pizza365/vouchers_detail", printCourseURLMiddleware, getAllVoucher);

router.get("/devcamp-pizza365/users", printCourseURLMiddleware, getAllUser);

router.get("/devcamp-pizza365/vouchers/:maVoucher", printCourseURLMiddleware, getVoucherByMaVoucher);

// Export dữ liệu 1 module
module.exports = router;


