// Import bo thu vien express
const express = require('express');

// Import Course Middleware
const { printCourseURLMiddleware } = require('../middlewares/courseMiddlewares.js');

const router = express.Router();

// Import drink Controller
const { createDrinkOfOrder, getAllDrinkOfOrder, getDrinkById, updateDrinkById, deleteDrinkById } = require("../controllers/drinkController.js")

router.get("/orders/:orderId/drinks", printCourseURLMiddleware, getAllDrinkOfOrder);

router.post("/orders/:orderId/drinks", printCourseURLMiddleware, createDrinkOfOrder);

router.get("/drinks/:drinkId", printCourseURLMiddleware, getDrinkById);

router.put("/drinks/:drinkId", printCourseURLMiddleware, updateDrinkById);

router.delete("/orders/:orderId/drinks/:drinkId", printCourseURLMiddleware, deleteDrinkById);

// Export dữ liệu 1 module
module.exports = router;