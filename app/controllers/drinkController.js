// Import drink model vào controller
const drinkModel = require("../models/drinkModel.js");
const orderModel = require("../models/orderModel.js");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createDrinkOfOrder = (request, response) => {
    // B1: Thu thập dữ liệu
    let orderId = request.params.orderId

    let bodyRequest = request.body;
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(orderId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order Id is invalid"
        })
    }

    if (!bodyRequest.maNuocUong) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Mã nước uống is required"
        })
    }

    if (!bodyRequest.tenNuocUong) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Tên nước uống is required"
        })
    }

    if (!bodyRequest.donGia) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Đơn giá is required"
        })
    }

    if (!(Number.isInteger(bodyRequest.donGia))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Đơn giá is not valid"
        })
    }


    // B3: Thao tác với cơ sở dữ liệu
    let newDrinkInput = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: bodyRequest.maNuocUong,
        tenNuocUong: bodyRequest.tenNuocUong,
        donGia: bodyRequest.donGia
    }

    drinkModel.create(newDrinkInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            orderModel.findByIdAndUpdate(orderId,
                {
                    $push: { drink: data._id }
                },
                (err, updatedOrder) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal sever error",
                            message: error.message
                        })
                    } else {
                        return response.status(201).json({
                            status: "Create Drink Success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}

const getAllDrinkOfOrder = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(orderId)
        .populate("drink")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get data success",
                    data: data.drink
                })
            }
        })
}

const getDrinkById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let drinkId = request.params.drinkId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(drinkId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Drink ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    drinkModel.findById(drinkId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get Drink By Id: " + drinkId,
                data: data
            })
        }
    })


}

const updateDrinkById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let drinkId = request.params.drinkId
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(drinkId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Drink ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let drinkUpdate = {
        maNuocUong: bodyRequest.maNuocUong,
        tenNuocUong: bodyRequest.tenNuocUong,
        donGia: bodyRequest.donGia
    }

    drinkModel.findByIdAndUpdate(drinkId, drinkUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Update Drink success",
                data: data
            })
        }
    })
}

const deleteDrinkById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    let drinkId = request.params.drinkId;
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(orderId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is not valid"
        })
    }
    if (!(mongoose.Types.ObjectId.isValid(drinkId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Drink ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    drinkModel.findByIdAndDelete(drinkId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            // Sau khi xóa xong 1 review khỏi collection cần xóa thêm drinkId trong order đang chứa nó
            orderModel.findByIdAndUpdate(orderId,
                {
                    $pull: { drink: drinkId }
                },
                (err, updatedOrder) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return response.status(204).json({
                            status: "Success: Delete Order success"
                        })
                    }
                })
        }
    })
}

// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createDrinkOfOrder: createDrinkOfOrder,
    getAllDrinkOfOrder: getAllDrinkOfOrder,
    getDrinkById: getDrinkById,
    updateDrinkById: updateDrinkById,
    deleteDrinkById: deleteDrinkById,
}