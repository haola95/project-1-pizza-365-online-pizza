// Import voucher model vào controller
const voucherModel = require("../models/voucherModel.js");
const orderModel = require("../models/orderModel.js");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createVoucherOfOrder = (request, response) => {
    // B1: Thu thập dữ liệu
    let orderId = request.params.orderId

    let bodyRequest = request.body;
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(orderId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order Id is invalid"
        })
    }

    if (!bodyRequest.maVoucher) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Mã voucher is required"
        })
    }

    if (!bodyRequest.phanTramGiamGia) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Phần trăm giảm giá is required"
        })
    }

    if (!(Number.isInteger(bodyRequest.phanTramGiamGia))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Phần trăm giảm giá is not valid"
        })
    }


    // B3: Thao tác với cơ sở dữ liệu
    let newVoucherInput = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: bodyRequest.maVoucher,
        phanTramGiamGia: bodyRequest.phanTramGiamGia,
        ghiChu: bodyRequest.ghiChu,
    }

    voucherModel.create(newVoucherInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            orderModel.findByIdAndUpdate(orderId,
                {
                    $push: { voucher: data._id }
                },
                (err, updatedOrder) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal sever error",
                            message: error.message
                        })
                    } else {
                        return response.status(201).json({
                            status: "Create Voucher Success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}

const getAllVoucherOfOrder = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(orderId)
        .populate("voucher")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get data success",
                    data: data.voucher
                })
            }
        })
}

const getVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get Voucher By Id: " + voucherId,
                data: data
            })
        }
    })


}

const updateVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let voucherUpdate = {
        maVoucher: bodyRequest.maVoucher,
        phanTramGiamGia: bodyRequest.phanTramGiamGia,
        ghiChu: bodyRequest.ghiChu,
    }

    voucherModel.findByIdAndUpdate(voucherId, voucherUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Update Voucher success",
                data: data
            })
        }
    })
}

const deleteVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let orderId = request.params.orderId
    let voucherId = request.params.voucherId;
    
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(orderId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is not valid"
        })
    }
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    voucherModel.findByIdAndDelete(voucherId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            // Sau khi xóa xong 1 review khỏi collection cần xóa thêm voucherId trong order đang chứa nó
            orderModel.findByIdAndUpdate(orderId,
                {
                    $pull: { voucher: voucherId }
                },
                (err, updatedOrder) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return response.status(204).json({
                            status: "Success: Delete Order success"
                        })
                    }
                })
        }
    })
}

// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createVoucherOfOrder: createVoucherOfOrder,
    getAllVoucherOfOrder: getAllVoucherOfOrder,
    getVoucherById: getVoucherById,
    updateVoucherById: updateVoucherById,
    deleteVoucherById: deleteVoucherById
}