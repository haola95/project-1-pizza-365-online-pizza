// Import user model vào controller
const userModel = require("../models/userModel.js");
const orderModel = require("../models/orderModel.js");
const drinkModel = require('../models/drinkModel.js')
const voucherModel = require('../models/voucherModel.js')

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let fullName = request.body.fullName;
    let email = request.body.email;
    let address = request.body.address;
    let phone = request.body.phone;
    let pizzaType = request.body.pizzaType;
    let pizzaSize = request.body.menuName;
    let suon = request.body.suon;
    let duongKinh = request.body.duongKinh;
    let salad = request.body.salad;
    let priceVND = request.body.priceVND;
    let soLuongNuoc = request.body.soLuongNuoc;
    let maNuocUong = request.body.maNuocUong;
    let maVoucher = request.body.maVoucher;
    let status = "OPEN";
    // Random 1 orderCode bất kỳ
    let orderCode = mongoose.Types.ObjectId();

    let createUser = {
        _id: mongoose.Types.ObjectId(),
        fullName: fullName,
        email: email,
        address: address,
        phone: phone
    }

    let createOrder = {
        _id: mongoose.Types.ObjectId(),
        orderCode: orderCode,
        pizzaSize: pizzaSize,
        pizzaType: pizzaType,
        status: status
    }

    var gSelectedSizeMenu = {
        menuName: "",    // S, M, L
        duongKinhCM: "",
        suonnuong: "",
        saladGr: "",
        drink: "",
        priceVND: ""
    }

    if (pizzaSize === "Large") {
        gSelectedSizeMenu.menuName = "Large";
        gSelectedSizeMenu.duongKinhCM = "30cm";
        gSelectedSizeMenu.suonnuong = 8;
        gSelectedSizeMenu.saladGr = "500g";
        gSelectedSizeMenu.drink = 4;
        gSelectedSizeMenu.priceVND = 250000;
    } else if (pizzaSize === "Small") {
        gSelectedSizeMenu.menuName = "Small";
        gSelectedSizeMenu.duongKinhCM = "20cm";
        gSelectedSizeMenu.suonnuong = 2;
        gSelectedSizeMenu.saladGr = "200g";
        gSelectedSizeMenu.drink = 2;
        gSelectedSizeMenu.priceVND = 150000;
    } else if (pizzaSize === "Small") {
        gSelectedSizeMenu.menuName = "Small";
        gSelectedSizeMenu.duongKinhCM = "25cm";
        gSelectedSizeMenu.suonnuong = 4;
        gSelectedSizeMenu.saladGr = "300g";
        gSelectedSizeMenu.drink = 3;
        gSelectedSizeMenu.priceVND = 200000;
    }

    // B2: Validate dữ liệu từ request body
    if (!fullName) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Full Name is required"
        })
    }

    if (!email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Email is required"
        })
    }

    if (!address) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Address is required"
        })
    }

    if (!phone) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Phone is required"
        })
    }

    if (!pizzaSize) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "pizzaSize is required"
        })
    }

    if (!pizzaType) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Pizza Type is required"
        })
    }

    if (!maNuocUong) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "maNuocUong is required"
        })
    }

    // Sử dụng userModel tìm kiếm bằng email
    userModel.findOne({ email: email }, (errorFindUser, userExist) => {
        if (errorFindUser) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindUser.message
            })
        } else {
            if (!userExist) {
                // Nếu user không tồn tại trong hệ thống
                // Tạo user mới
                userModel.create(createUser, (errCreateUser, userCreated) => {
                    if (errCreateUser) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: errCreateUser.message
                        })
                    } else {
                        orderModel.create(createOrder, (errorCreateOrder, dataCreateOrder) => {
                            if (errorCreateOrder) {
                                return response.status(500).json({
                                    status: "Error 500: Internal sever error",
                                    message: errorCreateOrder.message
                                })
                            } else {
                                userModel.findByIdAndUpdate(userCreated._id,
                                    {
                                        $push: { orders: dataCreateOrder._id }
                                    },
                                    (errorUpdatedUser, dataUpdatedUser) => {
                                        if (errorUpdatedUser) {
                                            return response.status(500).json({
                                                status: "Error 500: Internal sever error",
                                                message: errorUpdatedUser.message
                                            })
                                        } else {
                                            // push DrinkId vào Order 
                                            var conditionDrink = { maNuocUong: maNuocUong }
                                            drinkModel.find(conditionDrink, (errorDrink, dataDrink) => {
                                                if (errorDrink) {
                                                    return response.status(500).json({
                                                        status: "Error 500: Internal sever error",
                                                        message: errorDrink.message
                                                    })
                                                } else {
                                                    orderModel.findByIdAndUpdate(dataCreateOrder._id,
                                                        {
                                                            $push: { drink: dataDrink[0]._id }
                                                        },
                                                        (errorUpdatedUser, dataUpdatedUser) => {
                                                            if (errorUpdatedUser) {
                                                                return response.status(500).json({
                                                                    status: "Error 500: Internal sever error",
                                                                    message: errorUpdatedUser.message
                                                                })
                                                            } else {
                                                                //Nếu Mã Voucher không tồn tại
                                                                if (!maVoucher) {
                                                                    orderModel.findByIdAndUpdate(dataCreateOrder._id,
                                                                        {
                                                                            $push: { voucher: null }
                                                                        },
                                                                        (errorUpdatedOrder, dataUpdatedOrder) => {
                                                                            if (errorUpdatedOrder) {
                                                                                return response.status(500).json({
                                                                                    status: "Error 500: Internal sever error",
                                                                                    message: errorUpdatedOrder.message
                                                                                })
                                                                            } else {
                                                                                return response.status(201).json({
                                                                                    _id: dataCreateOrder._id,
                                                                                    orderId: orderCode,
                                                                                    pizzaType: pizzaType,
                                                                                    kichCo: pizzaSize,
                                                                                    duongKinh: duongKinh,
                                                                                    suon: suon,
                                                                                    salad: salad,
                                                                                    maNuocUong: maNuocUong,
                                                                                    soLuongNuoc: soLuongNuoc,
                                                                                    fullName: fullName,
                                                                                    email: email,
                                                                                    address: address,
                                                                                    phone: phone,
                                                                                    maVoucher: null,
                                                                                    giamGia: 0,
                                                                                    thanhTien: priceVND,
                                                                                    trangThai: status,
                                                                                    ngayTao: dataCreateOrder.ngayTao,
                                                                                    ngayCapNhat: dataCreateOrder.ngayCapNhat,
                                                                                })
                                                                            }
                                                                        }
                                                                    )
                                                                } else {

                                                                    // Nếu Mã Voucher tồn tại
                                                                    let condtionMaVoucher = { maVoucher }
                                                                    voucherModel.findOne(condtionMaVoucher, (errorFindVoucher, voucherExist) => {
                                                                        if (errorFindVoucher) {
                                                                            return response.status(500).json({
                                                                                status: "Error 500: Internal sever error",
                                                                                message: errorFindVoucher.message
                                                                            })
                                                                        } else {
                                                                            // Nếu Mã voucher không có trong Voucher Modal
                                                                            if (!voucherExist) {
                                                                                orderModel.findByIdAndUpdate(dataCreateOrder._id,
                                                                                    {
                                                                                        $push: { voucher: null }
                                                                                    },
                                                                                    (errorUpdatedOrder, dataUpdatedOrder) => {
                                                                                        if (errorUpdatedOrder) {
                                                                                            return response.status(500).json({
                                                                                                status: "Error 500: Internal sever error",
                                                                                                message: errorUpdatedOrder.message
                                                                                            })
                                                                                        } else {
                                                                                            response.status(201).json({
                                                                                                _id: dataCreateOrder._id,
                                                                                    orderId: orderCode,
                                                                                    pizzaType: pizzaType,
                                                                                    kichCo: pizzaSize,
                                                                                    duongKinh: duongKinh,
                                                                                    suon: suon,
                                                                                    salad: salad,
                                                                                    maNuocUong: maNuocUong,
                                                                                    soLuongNuoc: soLuongNuoc,
                                                                                    fullName: fullName,
                                                                                    email: email,
                                                                                    address: address,
                                                                                    phone: phone,
                                                                                    maVoucher: null,
                                                                                    giamGia: 0,
                                                                                    thanhTien: priceVND,
                                                                                    trangThai: status,
                                                                                    ngayTao: dataCreateOrder.ngayTao,
                                                                                    ngayCapNhat: dataCreateOrder.ngayCapNhat
                                                                                            })
                                                                                        }
                                                                                    })
                                                                            } else {
                                                                                // Nếu Mã voucher có trong Voucher Modal
                                                                                // Check Phần trăm giảm giá
                                                                                orderModel.findByIdAndUpdate(dataCreateOrder._id,
                                                                                    {
                                                                                        $push: { voucher: voucherExist._id }
                                                                                    },
                                                                                    (errorUpdatedOrder, dataUpdatedOrder) => {
                                                                                        if (errorUpdatedOrder) {
                                                                                            return response.status(500).json({
                                                                                                status: "Error 500: Internal sever error",
                                                                                                message: errorUpdatedOrder.message
                                                                                            })
                                                                                        } else {
                                                                                            console.log(voucherExist)
                                                                                            let giamGia = priceVND * ((voucherExist.phanTramGiamGia) / 100);
                                                                                            return response.status(201).json({
                                                                                                _id: dataCreateOrder._id,
                                                                                    orderId: orderCode,
                                                                                    pizzaType: pizzaType,
                                                                                    kichCo: pizzaSize,
                                                                                    duongKinh: duongKinh,
                                                                                    suon: suon,
                                                                                    salad: salad,
                                                                                    maNuocUong: maNuocUong,
                                                                                    soLuongNuoc: soLuongNuoc,
                                                                                    fullName: fullName,
                                                                                    email: email,
                                                                                    address: address,
                                                                                    phone: phone,
                                                                                    maVoucher: voucherExist.maVoucher,
                                                                                    giamGia: giamGia,
                                                                                    thanhTien: priceVND,
                                                                                    trangThai: status,
                                                                                    ngayTao: dataCreateOrder.ngayTao,
                                                                                    ngayCapNhat: dataCreateOrder.ngayCapNhat
                                                                                            })
                                                                                        }
                                                                                    })
                                                                            }
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        })
                                                }
                                            })
                                        }
                                    })
                            }
                        })
                    }
                })
            } else {
                // Nếu user đã tồn tại trong hệ thống
                // lấy id của User tạo order
                orderModel.create(createOrder, (errorCreateOrder, dataCreateOrder) => {
                    if (errorCreateOrder) {
                        return response.status(500).json({
                            status: "Error 500: Internal sever error",
                            message: errorCreateOrder.message
                        })
                    } else {
                        userModel.findByIdAndUpdate(userExist._id,
                            {
                                $push: { orders: dataCreateOrder._id }
                            },
                            (errorUpdatedUser, dataUpdatedUser) => {
                                if (errorUpdatedUser) {
                                    return response.status(500).json({
                                        status: "Error 500: Internal sever error",
                                        message: errorUpdatedUser.message
                                    })
                                } else {
                                    // push DrinkId vào Order 
                                    var conditionDrink = { maNuocUong: maNuocUong }
                                    drinkModel.find(conditionDrink, (errorDrink, dataDrink) => {
                                        if (errorDrink) {
                                            return response.status(500).json({
                                                status: "Error 500: Internal sever error",
                                                message: errorDrink.message
                                            })
                                        } else {
                                            orderModel.findByIdAndUpdate(dataCreateOrder._id,
                                                {
                                                    $push: { drink: dataDrink[0]._id }
                                                },
                                                (errorUpdatedUser, dataUpdatedUser) => {
                                                    if (errorUpdatedUser) {
                                                        return response.status(500).json({
                                                            status: "Error 500: Internal sever error",
                                                            message: errorUpdatedUser.message
                                                        })
                                                    } else {
                                                        //Nếu Mã Voucher không tồn tại
                                                        if (!maVoucher) {
                                                            orderModel.findByIdAndUpdate(dataCreateOrder._id,
                                                                {
                                                                    $push: { voucher: null }
                                                                },
                                                                (errorUpdatedOrder, dataUpdatedOrder) => {
                                                                    if (errorUpdatedOrder) {
                                                                        return response.status(500).json({
                                                                            status: "Error 500: Internal sever error",
                                                                            message: errorUpdatedOrder.message
                                                                        })
                                                                    } else {
                                                                        return response.status(201).json({
                                                                            _id: dataCreateOrder._id,
                                                                            orderId: orderCode,
                                                                            pizzaType: pizzaType,
                                                                            kichCo: pizzaSize,
                                                                            duongKinh: duongKinh,
                                                                            suon: suon,
                                                                            salad: salad,
                                                                            maNuocUong: maNuocUong,
                                                                            soLuongNuoc: soLuongNuoc,
                                                                            fullName: fullName,
                                                                            email: email,
                                                                            address: address,
                                                                            phone: phone,
                                                                            maVoucher: null,
                                                                            giamGia: 0,
                                                                            thanhTien: priceVND,
                                                                            trangThai: status,
                                                                            ngayTao: dataCreateOrder.ngayTao,
                                                                            ngayCapNhat: dataCreateOrder.ngayCapNhat,
                                                                        })
                                                                    }
                                                                }
                                                            )
                                                        } else {

                                                            // Nếu Mã Voucher tồn tại
                                                            let condtionMaVoucher = { maVoucher }
                                                            voucherModel.findOne(condtionMaVoucher, (errorFindVoucher, voucherExist) => {
                                                                if (errorFindVoucher) {
                                                                    return response.status(500).json({
                                                                        status: "Error 500: Internal sever error",
                                                                        message: errorFindVoucher.message
                                                                    })
                                                                } else {
                                                                    // Nếu Mã voucher không có trong Voucher Modal
                                                                    if (!voucherExist) {
                                                                        orderModel.findByIdAndUpdate(dataCreateOrder._id,
                                                                            {
                                                                                $push: { voucher: null }
                                                                            },
                                                                            (errorUpdatedOrder, dataUpdatedOrder) => {
                                                                                if (errorUpdatedOrder) {
                                                                                    return response.status(500).json({
                                                                                        status: "Error 500: Internal sever error",
                                                                                        message: errorUpdatedOrder.message
                                                                                    })
                                                                                } else {
                                                                                    response.status(201).json({
                                                                                        _id: dataCreateOrder._id,
                                                                            orderId: orderCode,
                                                                            pizzaType: pizzaType,
                                                                            kichCo: pizzaSize,
                                                                            duongKinh: duongKinh,
                                                                            suon: suon,
                                                                            salad: salad,
                                                                            maNuocUong: maNuocUong,
                                                                            soLuongNuoc: soLuongNuoc,
                                                                            fullName: fullName,
                                                                            email: email,
                                                                            address: address,
                                                                            phone: phone,
                                                                            maVoucher: null,
                                                                            giamGia: 0,
                                                                            thanhTien: priceVND,
                                                                            trangThai: status,
                                                                            ngayTao: dataCreateOrder.ngayTao,
                                                                            ngayCapNhat: dataCreateOrder.ngayCapNhat
                                                                                    })
                                                                                }
                                                                            })
                                                                    } else {
                                                                        // Nếu Mã voucher có trong Voucher Modal
                                                                        // Check Phần trăm giảm giá
                                                                        orderModel.findByIdAndUpdate(dataCreateOrder._id,
                                                                            {
                                                                                $push: { voucher: voucherExist._id }
                                                                            },
                                                                            (errorUpdatedOrder, dataUpdatedOrder) => {
                                                                                if (errorUpdatedOrder) {
                                                                                    return response.status(500).json({
                                                                                        status: "Error 500: Internal sever error",
                                                                                        message: errorUpdatedOrder.message
                                                                                    })
                                                                                } else {
                                                                                    console.log(voucherExist)
                                                                                    let giamGia = priceVND * ((voucherExist.phanTramGiamGia) / 100);
                                                                                    return response.status(201).json({
                                                                                        _id: dataCreateOrder._id,
                                                                            orderId: orderCode,
                                                                            pizzaType: pizzaType,
                                                                            kichCo: pizzaSize,
                                                                            duongKinh: duongKinh,
                                                                            suon: suon,
                                                                            salad: salad,
                                                                            maNuocUong: maNuocUong,
                                                                            soLuongNuoc: soLuongNuoc,
                                                                            fullName: fullName,
                                                                            email: email,
                                                                            address: address,
                                                                            phone: phone,
                                                                            maVoucher: voucherExist.maVoucher,
                                                                            giamGia: giamGia,
                                                                            thanhTien: priceVND,
                                                                            trangThai: status,
                                                                            ngayTao: dataCreateOrder.ngayTao,
                                                                            ngayCapNhat: dataCreateOrder.ngayCapNhat
                                                                                    })
                                                                                }
                                                                            })
                                                                    }
                                                                }
                                                            })
                                                        }
                                                    }
                                                })
                                        }
                                    })
                                }
                            })
                    }
                })

            }
        }
    })
}

const getAllOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Thao tác với cơ sở dữ liệu
    orderModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json(
                data
            )
        }
    }).populate("voucher")
    .populate("drink")
}

const getOrderById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let orderCode = request.params.orderCode
    // B2: Validate dữ liệu
    // B3: Thao tác với cơ sở dữ liệu
    orderModel.find(orderId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get Order By Id: " + orderId,
                data: data
            })
        }
    })


}

const createDrink = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!bodyRequest.maNuocUong) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Mã nước uống is required"
        })
    }

    if (!bodyRequest.tenNuocUong) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Tên nước uống is required"
        })
    }

    if (!bodyRequest.donGia) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Đơn giá is required"
        })
    }

    if (!(Number.isInteger(bodyRequest.donGia))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Đơn giá is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let createDrink = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: bodyRequest.maNuocUong,
        tenNuocUong: bodyRequest.tenNuocUong,
        donGia: bodyRequest.donGia
    }

    drinkModel.create(createDrink, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Created New Drink Success ",
                data: data
            })
        }

    })
}

const getAllDrink = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    drinkModel.find((error, dataDrink) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json(dataDrink)
        }
    })
}

const createVoucher = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!bodyRequest.maVoucher) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Mã voucher is required"
        })
    }

    if (!bodyRequest.phanTramGiamGia) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Phần trăm giảm giá is required"
        })
    }

    if (!(Number.isInteger(bodyRequest.phanTramGiamGia))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Phần trăm giảm giá is not valid"
        })
    }
    
    // B3: Thao tác với cơ sở dữ liệu
    let createVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: bodyRequest.maVoucher,
        phanTramGiamGia: bodyRequest.phanTramGiamGia,
        ghiChu: bodyRequest.ghiChu,
    }

    voucherModel.create(createVoucher, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Created User Success ",
                voucher: data
            })
        }

    })
}

const getAllVoucher = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let maVoucher = request.query.maVoucher;

    let condition = {};
    if(maVoucher) {
        condition.maVoucher = maVoucher;
    }

    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    voucherModel.find(condition, (error, dataVoucher) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json(dataVoucher )
        }
    })
}

const getAllUser = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    userModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                Users: data,
                status: "Success 200: Get All User"
            })
        }
    })
}

const getVoucherByMaVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let maVoucher = request.params.maVoucher

    let conditionMaVoucher = { maVoucher };
    // B2: Validate dữ liệu
    // B3: Thao tác với cơ sở dữ liệu
    voucherModel.find(conditionMaVoucher, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json(
                data[0]
            )
        }
    })


}


// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createOrder: createOrder,
    createDrink: createDrink,
    getAllDrink: getAllDrink,
    getAllOrder: getAllOrder,
    getAllVoucher: getAllVoucher,
    getVoucherByMaVoucher: getVoucherByMaVoucher,
    createVoucher: createVoucher,
    getAllUser: getAllUser



}