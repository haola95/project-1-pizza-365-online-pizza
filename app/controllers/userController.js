// Import user model vào controller
const userModel = require("../models/userModel.js");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createUser = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!bodyRequest.fullName) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Full Name is required"
        })
    }

    if (!bodyRequest.email) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Email is required"
        })
    }

    if (!bodyRequest.address) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Address is required"
        })
    }

    if (!bodyRequest.phone) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Phone is required"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let createUser = {
        _id: mongoose.Types.ObjectId(),
        fullName: bodyRequest.fullName,
        email: bodyRequest.email,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
    }

    userModel.create(createUser, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Created User Success ",
                data: data
            })
        }

    })
}

const getAllUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Thao tác với cơ sở dữ liệu
    userModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All User",
                data: data
            })
        }
    })
}

const getUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let userId = request.params.userId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get User By Id: " + userId,
                data: data
            })
        }
    })


}

const updateUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let userId = request.params.userId
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let userUpdate = {
        fullName: bodyRequest.fullName,
        email: bodyRequest.email,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
    }

    userModel.findByIdAndUpdate(userId, userUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Update User success",
                data: data
            })
        }
    })
}

const deleteUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let userId = request.params.userId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Delete User Success "
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createUser: createUser,
    getAllUser: getAllUser,
    getUserById: getUserById,
    updateUserById: updateUserById,
    deleteUserById: deleteUserById
}